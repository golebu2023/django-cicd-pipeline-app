#!/usr/bin/env groovy

def increment(){
    echo "increment..."
    env.WORKSPACE = pwd()
    matcher = readFile("${env.WORKSPACE}/version.xml")
    def list = matcher.split(",")
    major = list[0]
    minor = list[1] 
    patch = list[2]
    patch = patch as Integer
    patch = patch + 1
}
return this


def buildPush(){
    echo "building app..."
    sh "docker build --tag $IMAGE_NAME_TAG-${major}.${minor}.${patch} ."
    withCredentials([usernamePassword(
                    credentialsId: "dockerhub-credentials", 
                    usernameVariable: "USER", 
                    passwordVariable: "PASS")])
    {
        sh "docker login -u ${USER} -p ${PASS}"
        sh "docker push $IMAGE_NAME_TAG-${major}.${minor}.${patch}"
        writeFile (file: "${env.WORKSPACE}/version.xml", 
            text: "${major},${minor},${patch}", encoding: "UTF-8")
    }
}
return this


def deploy(){
    echo "deploying...d.eploying....c..deploying..."
}
return this


def updateCommit(){
    echo "Update commit..."
    patch = patch as Integer
    if (patch == 0){
      sh "git config --global user.email 'jenkins@gmail.com'"
      sh "git config --global user.name 'jenkins-server'"
      sh "git config --list"
    }
  
    withCredentials([usernamePassword(credentialsId: "gitlab-credentials", usernameVariable: "USER", passwordVariable: "PASS")]){
        sh "git add ."
        sh "git commit -m 'ci: jenkins version modified'" 
        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/golebu2023/django-cicd-pipeline-app.git"
        sh "git push origin HEAD:main"
    }
}
return this